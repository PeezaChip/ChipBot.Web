﻿namespace ChipBot.Web.Shared.Api
{
    public class IonotekaQueue
    {
        public ChipTrackApi CurrentTrack { get; set; }
        public ChipTrackApi[] Queue { get; set; }
    }
}
