﻿using Discord.WebSocket;
using System;

namespace ChipBot.Web.Shared.Api
{
    public class ApiRole
    {
        public uint Color { get; set; }

        public bool IsHoisted { get; set; }

        public bool IsManaged { get; set; }

        public bool IsMentionable { get; set; }

        public string Name { get; set; }

        public int Position { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public ulong Id { get; set; }

        public string Mention { get; set; }

        public ApiRole() { }

        public ApiRole(SocketRole role)
        {
            Color = role.Color.RawValue;
            IsHoisted = role.IsHoisted;
            IsManaged = role.IsManaged;
            IsMentionable = role.IsMentionable;
            Name = role.Name;
            Position = role.Position;
            CreatedAt = role.CreatedAt;
            Id = role.Id;
            Mention = role.Mention;
        }

        public static ApiRole UnknownRole => new() { Id = 0, Name = "Unknown", Color = Discord.Color.DarkerGrey.RawValue };
    }
}
