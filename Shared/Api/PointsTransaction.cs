﻿using System;

namespace ChipBot.Web.Shared.Api
{
    public class PointsTransaction
    {
        public string Reason { get; set; }
        public DateTime DateTime { get; set; }

        public long Points { get; set; }
    }
}
