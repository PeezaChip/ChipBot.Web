﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace ChipBot.Web.Shared.Api
{
    public class ApiUser
    {
        public string AvatarId { get; set; }

        public string Discriminator { get; set; }

        public ushort DiscriminatorValue { get; set; }

        public bool IsBot { get; set; }

        public bool IsWebhook { get; set; }
        public bool IsStreaming { get; set; }

        public string Username { get; set; }
        public string Nickname { get; set; }

        public UserProperties? PublicFlags { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? JoinedAt { get; set; }

        public ulong Id { get; set; }

        public string Mention { get; set; }

        //public IActivity Activity { get; set; }

        public UserStatus Status { get; set; }

        public IEnumerable<ClientType> ActiveClients { get; set; }

        //public IImmutableList<IActivity> Activities { get; set; }

        public string AvatarUrl { get; set; }
        public string DefaultAvatarUrl { get; set; }

        public ulong[] RoleIds { get; set; }

        public ApiUser() { }

        public ApiUser(SocketGuildUser user)
        {
            AvatarId = user.AvatarId;
            Discriminator = user.Discriminator;
            DiscriminatorValue = user.DiscriminatorValue;
            IsBot = user.IsBot;
            IsWebhook = user.IsWebhook;
            IsStreaming = user.VoiceState?.IsStreaming ?? false;
            Username = user.Username;
            Nickname = user.Username;
            PublicFlags = user.PublicFlags;
            CreatedAt = user.CreatedAt;
            JoinedAt = user.JoinedAt;
            Id = user.Id;
            Mention = user.Mention;
            //Activity = user.Activity;
            Status = user.Status;
            ActiveClients = user.ActiveClients;
            //Activities = user.Activities;
            AvatarUrl = user.GetAvatarUrl(ImageFormat.WebP, 512);
            DefaultAvatarUrl = user.GetDefaultAvatarUrl();
            RoleIds = user.Roles.Select(r => r.Id).ToArray();
        }

        public static ApiUser UnknownUser => new() { Id = 0, DiscriminatorValue = 0000, Discriminator = "0000", Nickname = "Unknown", Username = "Unknown" };
    }
}
