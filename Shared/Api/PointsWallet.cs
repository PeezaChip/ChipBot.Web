﻿using System.Collections.Generic;

namespace ChipBot.Web.Shared.Api
{
    public class PointsWallet
    {
        public long Balance { get; set; }
        public List<PointsTransaction> Transactions { get; set; }
    }
}
