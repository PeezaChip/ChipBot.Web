﻿using System;

namespace ChipBot.Web.Shared.SignalR
{
    public class LogMessage
    {
        public DateTime DateTime { get; set; }
        public string Severity { get; set; }
        public ushort SeverityId { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStack { get; set; }
        public string ExceptionSource { get; set; }
    }
}
