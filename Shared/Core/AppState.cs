﻿using ChipBot.Web.Shared.Api;

namespace ChipBot.Web.Shared.Core
{
    public class AppState
    {
        public string Name => User.Nickname ?? User.Username;
        public string Avatar => User.AvatarUrl;
        public ApiUser User { get; set; }
    }
}
