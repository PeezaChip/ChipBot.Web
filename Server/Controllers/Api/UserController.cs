﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Web.Server.Core.Constants;
using ChipBot.Web.Shared.Api;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ChipBot.Web.Server.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly DiscordSocketClient _discord;
        private readonly ServiceConfigurationProvider<Settings> _config;

        public UserController(DiscordSocketClient discord, ServiceConfigurationProvider<Settings> config)
        {
            _discord = discord;
            _config = config;
        }

        [HttpGet("users")]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        public IEnumerable<ApiUser> GetUsers()
        {
            var guild = _discord.GetGuild(_config.Config.Guilds.MainGuild);
            return guild.Users.Select(u => new ApiUser(u));
        }

        [HttpGet("roles")]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        public IEnumerable<ApiRole> GetRoles()
        {
            var guild = _discord.GetGuild(_config.Config.Guilds.MainGuild);
            return guild.Roles.Select(u => new ApiRole(u));
        }
    }
}
