﻿using ChipBot.Core;
using ChipBot.Core.Core.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ChipBot.Web.Server.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Owner")]
    public class ConfigController : ControllerBase
    {
        private readonly IRepository<DbConfigEntity, string> _configRepo;
        private readonly MemoryCache _cache;

        public ConfigController(IRepository<DbConfigEntity, string> configRepo, MemoryCache cache)
        {
            _configRepo = configRepo;
            _cache = cache;
        }

        // GET: api/<ConfigController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return _configRepo.GetAll().Select(c => c.Name);
        }

        // GET api/<ConfigController>/configId
        [HttpGet("{id}")]
        public string Get(string id)
        {
            return _configRepo.GetById(id).Configuration;
        }

        // POST api/<ConfigController>/configId
        [HttpPost("{id}")]
        public void Post(string id, [FromBody] string value)
        {
            _configRepo.Add(new() { Name = id, Configuration = value });
        }

        // PUT api/<ConfigController>/configId
        [HttpPut("{id}")]
        public void Put(string id, [FromBody] string value)
        {
            _configRepo.Update(new() { Name = id, Configuration = value });
            DropCache(id);
        }

        // DELETE api/<ConfigController>/configId
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _configRepo.Remove(id);
            DropCache(id);
        }

        private void DropCache(string configName)
        {
            _cache.Remove(ServiceConfiguration.FormCacheKey(configName));
        }
    }
}
