﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Web.Shared.Api;
using Discord;
using Discord.WebSocket;
using IonotekaPlugin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Victoria;

namespace ChipBot.Web.Server.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class IonotekaController : ControllerBase
    {
        private readonly DiscordSocketClient _discord;
        private readonly LavaService _lavaService;
        private readonly ServiceConfigurationProvider<Settings> _config;

        public IonotekaController(DiscordSocketClient discord, LavaService lavaService, ServiceConfigurationProvider<Settings> config)
        {
            _discord = discord;
            _lavaService = lavaService;
            _config = config;
        }

        [HttpGet("queue")]
        public async Task<IonotekaQueue> GetQueue()
        {
            var player = GetPlayer();
            return player == null
                ? null
                : new IonotekaQueue()
            {
                CurrentTrack = await FromChipTrack(player.Track is ChipTrack ? (ChipTrack)player.Track : new ChipTrack(player.Track, _discord.CurrentUser)),
                Queue = await Task.WhenAll(player.Queue
                    .Cast<ChipTrack>()
                    .Select(t => FromChipTrack(t)))
            };
        }

        private IGuild MainGuild => _discord.GetGuild(_config.Config.Guilds.MainGuild);

        private LavaPlayer GetPlayer()
        {
            var node = _lavaService.GetNode();
            if (node == null) return null;

            node.TryGetPlayer(MainGuild, out var player);
            return player;
        }

        private async Task<ChipTrackApi> FromChipTrack(ChipTrack track)
        {
            return new ChipTrackApi()
            {
                UserId = track.User.Id,
                Author = track.Author,
                Title = track.Title,
                Duration = track.Duration,
                Position = track.Position,
                Url = track.Url,
                Source = track.Source,
                Image = await track.FetchArtworkAsync()
            };
        }
    }
}
