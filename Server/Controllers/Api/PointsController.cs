﻿using ChipBot.Core;
using ChipBot.Core.Core.Database;
using ChipBot.Web.Server.Core.Constants;
using ChipBot.Web.Shared.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PointsPlugin;
using PointsPlugin.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ChipBot.Web.Server.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PointsController : ControllerBase
    {
        private readonly IRepository<PointsModel, ulong> _repository;
        private readonly IKeylessRepository<PointsTransactionModel> _transactionRepository;
        private readonly ServiceConfigurationProvider<PointsConfiguration> _config;

        public PointsController(IRepository<PointsModel, ulong> repository, IKeylessRepository<PointsTransactionModel> transactionRepository, ServiceConfigurationProvider<PointsConfiguration> config)
        {
            _repository = repository;
            _transactionRepository = transactionRepository;
            _config = config;
        }

        [HttpGet("currency")]
        [ResponseCache(CacheProfileName = CachingConstants.HourPublicCache)]
        public string GetCurrency()
        {
            return _config.Config.CurrencyName;
        }

        // GET: api/<PointsController>
        [HttpGet]
        [ResponseCache(CacheProfileName = CachingConstants.FiveMinutesPublicCache)]
        public Dictionary<ulong, long> Get()
        {
            return _repository.GetAll().ToDictionary(m => m.UserId, m => m.Points);
        }

        // GET api/<PointsController>/userId
        [HttpGet("{id}")]
        public long Get(ulong id)
        {
            return _repository.GetById(id).Points;
        }

        // GET api/<PointsController>/my
        [HttpGet("my")]
        public long GetMy()
        {
            return Get(ulong.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier)));
        }

        // POST api/<PointsController>/userId
        [HttpPost("{id}")]
        [Authorize(Roles = "Owner")]
        public void Post(ulong id, [FromBody] long value)
        {
            _repository.Add(new() { UserId = id, Points = value });
        }

        // PUT api/<PointsController>/userId
        [HttpPut("{id}")]
        [Authorize(Roles = "Owner")]
        public void Put(ulong id, [FromBody] long value)
        {
            _repository.Update(new() { UserId = id, Points = value });
        }

        // DELETE api/<PointsController>/userId
        [HttpDelete("{id}")]
        [Authorize(Roles = "Owner")]
        public void Delete(ulong id)
        {
            _repository.Remove(id);
        }

        [HttpGet("wallet")]
        public PointsWallet Transactions()
        {
            var userId = ulong.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            using var interaction = _transactionRepository.GetInteraction();
            var transactions = interaction.Set
                .Where(t => t.ExecutorId == userId)
                .OrderByDescending(t => t.DateTime)
                .Take(50).ToList();
            return new PointsWallet() { Balance = _repository.GetById(userId).Points, Transactions = transactions.Select(t => new PointsTransaction() { DateTime = t.DateTime, Points = t.Points, Reason = t.Reason }).ToList() };
        }
    }
}
