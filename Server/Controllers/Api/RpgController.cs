﻿using ChipBot.Web.Server.Core.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Static.Monsters.Unique;
using RPGPlugin.Core.Static.Quests;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;
using RPGPlugin.Services;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace ChipBot.Web.Server.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RpgController : ControllerBase
    {
        private readonly HeroProviderService _heroProvider;

        public RpgController(HeroProviderService heroProvider)
        {
            _heroProvider = heroProvider;
        }

        // GET api/<RpgController>/hero/userId
        [HttpGet("hero/{id}")]
        [ResponseCache(CacheProfileName = CachingConstants.MinuteUserCache)]
        public Hero Get(ulong id)
        {
            return _heroProvider.GetHero(id);
        }

        // GET api/<RpgController>/hero/my
        [HttpGet("hero/my")]
        public Hero GetMy()
        {
            return Get(ulong.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier)));
        }

        // GET api/<RpgController>/heroes
        [HttpGet("heroes")]
        [ResponseCache(CacheProfileName = CachingConstants.MinutePublicCache)]
        public IEnumerable<Hero> GetAll()
        {
            return _heroProvider.GetHeroes().Values;
        }

        [HttpGet("test")]
        public void Pepe()
        {
            var hero = new Hero()
            {
                Name = "Pepe",
                Avatar = "",
                RaceDescription = RaceDescriptions.Races["human"],
                RaceReference = (Race)Activator.CreateInstance(RaceDescriptions.Races["human"].Race),
                SpecDescription = SpecsDescriptions.Classes["warrior"],
                SpecReference = (Spec)Activator.CreateInstance(SpecsDescriptions.Classes["warrior"].Spec),
                UserID = 0
            };

            var dngHero = new DungeonHero(hero);
            var hitCount = 100;
            var dpsCreature = new DPSCheckCreature(hitCount);
            var logs = new List<string>();
            var fight = new Fight(new List<Creature>() { dngHero }, new List<Creature>() { dpsCreature }, logs.Add, new Random());
            fight.DoFight();
        }
    }
}
