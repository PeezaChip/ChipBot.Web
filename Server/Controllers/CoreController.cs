﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Web.Server.Core.Constants;
using ChipBot.Web.Shared.Api;
using ChipBot.Web.Shared.Core;
using Discord.WebSocket;
using Microsoft.AspNetCore.Mvc;

namespace ChipBot.Web.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CoreController : ControllerBase
    {
        private readonly DiscordSocketClient _discord;
        private readonly ServiceConfigurationProvider<Settings> _settings;

        public CoreController(DiscordSocketClient discord, ServiceConfigurationProvider<Settings> settings)
        {
            _discord = discord;
            _settings = settings;
        }

        [HttpGet("appstate")]
        [ResponseCache(CacheProfileName = CachingConstants.DayPublicCache)]
        public AppState GetAppState()
        {
            var botGuildUser = _discord.GetGuild(_settings.Config.Guilds.MainGuild).GetUser(_discord.CurrentUser.Id);
            return new()
            {
                User = new ApiUser(botGuildUser)
            };
        }
    }
}
