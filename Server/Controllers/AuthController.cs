﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Web.Server.Core;
using ChipBot.Web.Server.Core.Constants;
using ChipBot.Web.Shared.Auth;
using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChipBot.Web.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AuthController : ControllerBase
    {
        private readonly LoginTokenHandler _token;
        private readonly DiscordSocketClient _discord;
        private readonly ILogger<AuthController> _logger;
        private readonly ServiceConfigurationProvider<Settings> _settings;

        public AuthController(ILogger<AuthController> logger, LoginTokenHandler token, DiscordSocketClient discord, ServiceConfigurationProvider<Settings> settings)
        {
            _logger = logger;
            _token = token;
            _discord = discord;
            _settings = settings;
        }

        [HttpGet("login")]
        public async Task<IActionResult> Login()
        {
            var token = HttpContext.Request.Query["token"];
            if (string.IsNullOrWhiteSpace(token)) return Unauthorized();
            var user = _token.GetUserFromToken(token);
            if (user == null) return Unauthorized();

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(GetClaimsIdentity(user)), new AuthenticationProperties());
            return Redirect("/");
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/");
        }

        [HttpGet("userinfo")]
        [ResponseCache(CacheProfileName = CachingConstants.HourUserCache)]
        [Authorize]
        public UserInfo UserInfo()
        {
            return new()
            {
                Username = HttpContext.User.FindFirstValue(ClaimTypes.Name),
                UserId = ulong.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier)),
                Avatar = HttpContext.User.FindFirstValue("avatar"),
                Roles = HttpContext.User.FindAll(ClaimTypes.Role).Select(claim => claim.Value).ToArray()
            };
        }

        private ClaimsIdentity GetClaimsIdentity(IUser user)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, user.Username),
                new(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.UInteger64),
                new("avatar", user.GetAvatarUrl(size: 256))
            };

            AddRoles(claims, user);

            return new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
        }

        private void AddRoles(List<Claim> claims, IUser user)
        {
            if (_settings.Config.Owners.Contains(user.Id)) claims.Add(new(ClaimTypes.Role, "Owner"));

            var guildUser = _discord.GetGuild(_settings.Config.Guilds.MainGuild).GetUser(user.Id);
            if (_settings.Config.AllowedRoles.Any(eliteRoleId => guildUser.Roles.Select(role => role.Id).Contains(eliteRoleId))) claims.Add(new(ClaimTypes.Role, "Elite"));
        }
    }
}
