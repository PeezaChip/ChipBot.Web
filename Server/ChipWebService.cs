﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Core.Core.Classes;
using ChipBot.Core.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace ChipBot.Web.Server
{
    public class ChipWebService : ChipService, IWeb
    {
        private readonly ILoggerProvider _logProvider;
        private readonly ServiceConfigurationProvider<ChipWebConfig> _config;
        private readonly ServiceConfigurationProvider<Settings> _globalConfig;

        public ChipWebService(LoggingService log, ILoggerProvider logProvider, ServiceConfigurationProvider<ChipWebConfig> config, ServiceConfigurationProvider<Settings> globalConfig) : base(log)
        {
            _logProvider = logProvider;
            _config = config;
            _globalConfig = globalConfig;
        }

        public IServiceProvider StartWeb(Action<IServiceCollection> configureDelegate)
        {
            var web = Host.CreateDefaultBuilder()
                 .UseEnvironment(_globalConfig.Config.Debug.DebugMode ? "development" : "production")
                 .ConfigureLogging(logging =>
                 {
                     logging.ClearProviders();
                     logging.AddProvider(_logProvider);
                 })
                 .ConfigureServices(configureDelegate)
                 .ConfigureWebHostDefaults(webBuilder =>
                 {
                     webBuilder
                         .UseStartup<Startup>()
                         .UseUrls($"http://0.0.0.0:{_config.Config.Port}/");
                 })
                 .Build();

            web.RunAsync();

            return web.Services;
        }
    }
}
