﻿using Newtonsoft.Json;

namespace ChipBot.Web.Server
{
    public class ChipWebConfig
    {
        [JsonProperty("port")]
        public ushort Port { get; set; }

        [JsonProperty("realUrl")]
        public string RealUrl { get; set; }
    }
}
