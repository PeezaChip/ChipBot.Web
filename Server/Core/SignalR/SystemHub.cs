﻿using ChipBot.Core;
using ChipBot.Core.Core.Classes;
using ChipBot.Core.Extensions;
using ChipBot.Web.Shared.SignalR;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace ChipBot.Web.Server.Core.SignalR
{
    [Authorize(Roles = "Owner")]
    public class SystemHub : Hub {

        private readonly IBot _bot;
        private readonly IWeb _web;
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commandService;

        public SystemHub(IBot bot, IWeb web, DiscordSocketClient discord, CommandService commandService)
        {
            _bot = bot;
            _web = web;
            _discord = discord;
            _commandService = commandService;
        }

        public Task GetInfo()
        {
            var process = Process.GetCurrentProcess();

            var info = new SystemInfo()
            {
                CPU = RuntimeInformation.ProcessArchitecture.ToString(),
                OS = $"{RuntimeInformation.OSDescription} {RuntimeInformation.OSArchitecture}",
                ServerTime = DateTime.Now,
                ProcessStartTime = process.StartTime,
                MachineName = process.MachineName,
                Memory = Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString(),

                Runtime = RuntimeInformation.FrameworkDescription,
                DiscordNetVersion = DiscordConfig.Version,
                CoreVersion = ChipCore.Version,
                BotVersion = _bot.GetVersion(),
                WebVersion = _web.GetVersion(),

                GuildCount = _discord.Guilds.Count,
                Channels = _discord.Guilds.Sum(g => g.Channels.Count),
                Users = _discord.Guilds.Sum(g => g.Users.Count),
                Commands = _commandService.Commands.Count()
            };

            return Clients.Caller.SendAsync("InfoReceived", info);
        }
    }
}
