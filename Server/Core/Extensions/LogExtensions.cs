﻿using ChipBot.Core.Core.Models;
using ChipBot.Web.Shared.SignalR;

namespace ChipBot.Web.Server.Core.Extensions
{
    public static class LogExtensions
    {
        public static LogMessage ToSignalR(this ChipLogMessage message)
        {
            return new()
            {
                DateTime = message.DateTime,
                Severity = message.Severity,
                SeverityId = message.SeverityId,
                Message = message.Message,
                Source = message.Source,
                ExceptionMessage = message.ExceptionMessage,
                ExceptionSource = message.ExceptionSource,
                ExceptionStack = message.ExceptionStack
            };
        }
    }
}
