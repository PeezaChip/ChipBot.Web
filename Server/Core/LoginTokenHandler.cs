﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Security.Cryptography;

namespace ChipBot.Web.Server.Core
{
    public class LoginTokenHandler : ChipService
    {
        private readonly MemoryCache _cache;

        public LoginTokenHandler(LoggingService log, MemoryCache cache) : base(log)
        {
            _cache = cache;
        }

        public string GenerateLoginTokenForUser(IUser user)
        {
            var token = GenerateLoginToken();
            var key = GetCacheKey(token);

            _cache.Set(key, user, DateTimeOffset.Now.AddMinutes(5));
            return token;
        }

        public IUser GetUserFromToken(string token)
        {
            var key = GetCacheKey(token);

            if (_cache.TryGetValue<IUser>(key, out var user))
            {
                _cache.Remove(key);
                return user;
            }
            else
            {
                return null;
            }
        }

        private string GenerateLoginToken()
        {
            var bytes = RandomNumberGenerator.GetBytes(30);
            return WebEncoders.Base64UrlEncode(bytes);
        }

        private static string GetCacheKey(string token)
        {
            return $"chipweb_login_token_{token}";
        }
    }
}
