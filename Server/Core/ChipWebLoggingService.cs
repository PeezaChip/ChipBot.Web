﻿using ChipBot.Core.Core.Classes;
using ChipBot.Core.Core.Models;
using ChipBot.Web.Server.Core.Extensions;
using ChipBot.Web.Server.Core.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace ChipBot.Web.Server.Core
{
    public class ChipWebLoggingService : WebLoggingService
    {
        private readonly IServiceProvider serviceProvider;
        private IHubContext<SystemHub> systemHub;

        public ChipWebLoggingService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        protected override async Task WriteLog(ChipLogMessage msg)
        {
            await base.WriteLog(msg);

            if (systemHub == null)
            {
                systemHub = serviceProvider.GetService<IHubContext<SystemHub>>();
                if (systemHub == null) return;
            }

            await systemHub.Clients.All.SendAsync("LogReceived", msg.ToSignalR());
        }
    }
}
