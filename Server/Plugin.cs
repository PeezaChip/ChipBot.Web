﻿using ChipBot.Core;
using ChipBot.Core.Services;
using ChipBot.Web.Server.Core;
using ChipBot.Web.Server.Core.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ChipBot.Web.Server
{
    public class Plugin : AbstractPlugin
    {
        public override string Name => "ChipBot.Web.Server";

        public override void ModifyServiceCollection(Type[] allTypes, IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(WebLoggingFactoryMethod);
            serviceCollection.AddSingleton<LoggingService>(WebLoggingFactoryMethod);
        }

        public static ChipWebLoggingService WebLoggingFactoryMethod(IServiceProvider serviceProvider)
        {
            return new ChipWebLoggingService(serviceProvider);
        }
    }
}
