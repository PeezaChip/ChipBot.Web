﻿using ChipBot.Core;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Module;
using ChipBot.Web.Server.Core;
using Discord;
using Discord.Commands;
using System;
using System.Threading.Tasks;

namespace ChipBot.Web.Server
{
    [Group("Web")]
    [Alias("site")]
    [ChipCommandPrecondition(typeof(RequireChannel))]
    public class ChipWebModule : ChipCommandModuleBase
    {
        private readonly ServiceConfigurationProvider<ChipWebConfig> _config;
        private readonly LoginTokenHandler _token;

        public ChipWebModule(ServiceConfigurationProvider<ChipWebConfig> config, LoginTokenHandler token)
        {
            _config = config;
            _token = token;
        }

        [Command("login")]
        [Summary("Login to ChipWeb")]
        public async Task Login()
        {
            var dm = await Context.User.CreateDMChannelAsync();
            await dm.SendMessageAsync(embed: new EmbedBuilder()
                .WithTitle("ChipWeb")
                .WithCurrentTimestamp().WithColor(Color.DarkBlue)
                .WithThumbnailUrl(Context.Client.CurrentUser.GetAvatarUrl(ImageFormat.Auto, 512))
                .WithDescription($"To login to ChipWeb please follow this [link]({new Uri(new Uri(_config.Config.RealUrl), $"/auth/login?token={_token.GenerateLoginTokenForUser(Context.User)}").AbsoluteUri})\nLink can be used only once and is only available for 5 minutes.").Build());
        }
    }
}
