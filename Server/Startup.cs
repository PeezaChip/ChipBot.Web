using ChipBot.Web.Server.Core.Constants;
using ChipBot.Web.Server.Core.SignalR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;

namespace ChipBot.Web.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();

            services.AddControllersWithViews(opt =>
            {
                opt.CacheProfiles.Add(CachingConstants.NoCache, new() { NoStore = true, Duration = 0, Location = ResponseCacheLocation.None });

                opt.CacheProfiles.Add(CachingConstants.MinuteUserCache, new() { NoStore = false, Duration = 60, Location = ResponseCacheLocation.Client });
                opt.CacheProfiles.Add(CachingConstants.FiveMinutesUserCache, new() { NoStore = false, Duration = 60 * 5, Location = ResponseCacheLocation.Client });
                opt.CacheProfiles.Add(CachingConstants.HourUserCache, new() { NoStore = false, Duration = 60 * 60, Location = ResponseCacheLocation.Client });
                opt.CacheProfiles.Add(CachingConstants.DayUserCache, new() { NoStore = false, Duration = 60 * 60 * 24, Location = ResponseCacheLocation.Client });

                opt.CacheProfiles.Add(CachingConstants.MinutePublicCache, new() { NoStore = false, Duration = 60, Location = ResponseCacheLocation.Any });
                opt.CacheProfiles.Add(CachingConstants.FiveMinutesPublicCache, new() { NoStore = false, Duration = 60 * 5, Location = ResponseCacheLocation.Any });
                opt.CacheProfiles.Add(CachingConstants.HourPublicCache, new() { NoStore = false, Duration = 60 * 60, Location = ResponseCacheLocation.Any });
                opt.CacheProfiles.Add(CachingConstants.DayPublicCache, new() { NoStore = false, Duration = 60 * 60 * 24, Location = ResponseCacheLocation.Any });
            });
            services.AddRazorPages();

            services.AddSwaggerGen();

            services.AddSignalR();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "application/octet-stream" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseResponseCompression();

            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseCookiePolicy(new() { MinimumSameSitePolicy = SameSiteMode.Strict });

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();

                endpoints.MapHub<SystemHub>("/sr/system");
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
