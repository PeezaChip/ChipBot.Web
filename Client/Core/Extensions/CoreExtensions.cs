﻿using Discord;
using System;

namespace ChipBot.Web.Client.Core.Extensions
{
    public static class CoreExtensions
    {
        public static string ToHex(this uint color)
        {
            return color == 0 ? "var(--light)" : "#" + color.ToString("X").PadLeft(6, '0');
        }

        public static string ToTrackTime(this TimeSpan ts)
        {
            return $"{Math.Floor(ts.TotalMinutes):0}:{ts.Seconds:00}";
        }
    }
}
