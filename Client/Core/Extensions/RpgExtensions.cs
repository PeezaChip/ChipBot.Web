﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;

namespace ChipBot.Web.Client.Core.Extensions
{
    public static class RpgExtensions
    {
        public static string ToRarityClassName(this Item item)
        {
            return $"item-rarity-{item.Rarity.ToString().ToLower()}";
        }

        public static void FixDescriptions(this Hero hero)
        {
            if (hero.RaceDescription == null)
            {
                hero.RaceDescription = RaceDescriptions.Races[hero.Race.ToString()];
            }

            if (hero.SpecDescription == null)
            {
                hero.SpecDescription = SpecsDescriptions.Classes[hero.Class.ToString()];
            }
        }
    }
}
