﻿using ChipBot.Web.Shared.Auth;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChipBot.Web.Client.Core
{
    public class ChipTokenAuthProvider : AuthenticationStateProvider
    {
        private readonly HttpClient _httpClient;

        public ChipTokenAuthProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var uri = new Uri(_httpClient.BaseAddress, "/auth/userinfo");
            try
            {
                var data = await _httpClient.GetFromJsonAsync<UserInfo>(uri.AbsoluteUri);

                var claims = new List<Claim>() {
                    new Claim(ClaimTypes.Name, data.Username),
                    new Claim(ClaimTypes.NameIdentifier, data.UserId.ToString(), ClaimValueTypes.UInteger64),
                    new Claim("avatar", data.Avatar)
                };

                if (data.Roles != null && data.Roles.Any())
                {
                    foreach (var role in data.Roles)
                    {
                        claims.Add(new(ClaimTypes.Role, role));
                    }
                }

                var result = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity(claims, "ServerAuth")));

                return result;
            }
            catch
            {
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
            }
        }
    }
}
