﻿using ChipBot.Web.Client.Core.Services;
using ChipBot.Web.Shared.Api;
using System.Collections.Generic;
using System.Linq;

namespace ChipBot.Web.Client.Core.Models
{
    public class User : ApiUser
    {
        public List<ApiRole> Roles { get; private set; }

        public ApiRole Role => Roles?.OrderByDescending(r => r.Position).FirstOrDefault();

        public User(ApiUser user, UserCacheService service)
        {
            AvatarId = user.AvatarId;
            Discriminator = user.Discriminator;
            DiscriminatorValue = user.DiscriminatorValue;
            IsBot = user.IsBot;
            IsWebhook = user.IsWebhook;
            IsStreaming = user.IsStreaming;
            Username = user.Username;
            Nickname = user.Username;
            PublicFlags = user.PublicFlags;
            CreatedAt = user.CreatedAt;
            JoinedAt = user.JoinedAt;
            Id = user.Id;
            Mention = user.Mention;
            //Activity = user.Activity;
            Status = user.Status;
            ActiveClients = user.ActiveClients;
            //Activities = user.Activities;
            AvatarUrl = user.AvatarUrl ?? GetDefaultAvatar(user.DiscriminatorValue);
            DefaultAvatarUrl = user.DefaultAvatarUrl;
            RoleIds = user.RoleIds;

            Roles = user.RoleIds?
                .Select(r => service.GetRole(r))
                .ToList();
        }

        public static string GetDefaultAvatar(ushort discriminator)
        {
            return $"https://cdn.discordapp.com/embed/avatars/{discriminator % 5}.png";
        }
    }
}
