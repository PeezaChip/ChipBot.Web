﻿using ChipBot.Web.Client.Core.Models;
using ChipBot.Web.Shared.Api;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChipBot.Web.Client.Core.Services
{
    public class UserCacheService
    {
        private Dictionary<ulong, User> users = new();
        private Dictionary<ulong, ApiRole> roles = new();

        public User GetUser(ulong id)
        {
            Console.WriteLine($"GET USER {id}");
            Console.WriteLine($"GET USER {users.ContainsKey(id)}");
            return users.ContainsKey(id) ? users[id] : new User(ApiUser.UnknownUser, this);
        }

        public ApiRole GetRole(ulong id)
        {
            return roles.ContainsKey(id) ? roles[id] : ApiRole.UnknownRole;
        }

        public void RefreshUsersAndRoles(IEnumerable<ApiUser> apiUsers, IEnumerable<ApiRole> apiRoles)
        {
            Console.WriteLine("REFRESHED USERS");
            roles = apiRoles.ToDictionary(r => r.Id, r => r);
            users = apiUsers.Select(u => new User(u, this))
            .ToDictionary(u => u.Id, u => u);
        }
    }
}
