﻿using ChipBot.Web.Shared.Api;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ChipBot.Web.Client.Core.Services
{
    public class UserService
    {
        private static Task Task;

        private readonly HttpClient httpClient;
        private readonly UserCacheService userCache;

        public UserService(HttpClient httpClient, UserCacheService userCache)
        {
            this.httpClient = httpClient;
            this.userCache = userCache;
            if (Task == null) Task = RefreshUsersAndRoles();
        }

        private async Task RefreshUsersAndRoles()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("REFRESHING USERS");
                    userCache.RefreshUsersAndRoles(await httpClient.GetFromJsonAsync<List<ApiUser>>("api/user/users"), await httpClient.GetFromJsonAsync<List<ApiRole>>("api/user/roles"));
                    Console.WriteLine("REFRESHEDDDDD USERS");
                } catch (Exception ex)
                {
                    Console.WriteLine("REFRESGHH USERS ERROR");
                    Console.WriteLine(ex);
                }
                
                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }
    }
}

