﻿window.chipApp = {
    initMonaco: async function (element, text, lang) {
        require.config({ paths: { 'vs': 'js/monaco-editor/min/vs' } });
        return await new Promise((res, _) => {
            require(['vs/editor/editor.main'], function () {
                res(monaco.editor.create(element, {
                    value: text,
                    language: lang,
                    theme: 'vs-dark'
                }));
            });
        });
    },
    jsonFormat: function (json) {
        return JSON.stringify(JSON.parse(json), null, 2);
    }
};