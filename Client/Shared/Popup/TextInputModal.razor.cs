﻿using Blazorise;

namespace ChipBot.Web.Client.Shared.Popup
{
    public class TextInputSubmitEventArgs
    {
        public string Value { get; private set; }
        public ValidationStatus Status { get; set; }
        public string ErrorText { get; set; }

        public TextInputSubmitEventArgs(string value)
        {
            Status = ValidationStatus.None;
            Value = value;
            ErrorText = null;
        }
    }
}
