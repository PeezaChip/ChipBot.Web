using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using ChipBot.Web.Client.Core;
using ChipBot.Web.Client.Core.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChipBot.Web.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services
              .AddBlazorise(options =>
              {
                  
              })
              .AddBootstrapProviders()
              .AddFontAwesomeIcons();

            builder.Services.AddHttpClient("ChipBot.Web.ServerAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));
            //.AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

            // Supply HttpClient instances that include access tokens when making requests to the server project
            builder.Services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("ChipBot.Web.ServerAPI"));

            //builder.Services.AddOptions();
            builder.Services.AddAuthorizationCore();

            builder.Services.AddScoped<AuthenticationStateProvider, ChipTokenAuthProvider>();
            builder.Services.AddScoped(sp =>
            {
                return (ChipTokenAuthProvider)sp.GetRequiredService<AuthenticationStateProvider>();
            });

            builder.Services.AddScoped<UserService>();

            builder.Services.AddSingleton<UserCacheService>();

            await builder.Build().RunAsync();
        }
    }
}
